TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    keystring.cpp \
    info.cpp \
    sorter.cpp

HEADERS += \
    keystring.h \
    info.h \
    sorter.h

QMAKE_CXXFLAGS+= -fopenmp
QMAKE_LFLAGS +=  -fopenmp
