#include "info.h"

Info::Info()
{
    m_begin = clock(); // start measuring time
}

void Info::display()
{
    m_end = clock(); // stop measuring time
    std::cout << "The used method was " << m_method << std::endl;
    for(long i = 0; i < m_left.size(); i++)
        std::cout << m_left[i] << " and " << m_right[i] << " are similar" << std::endl;
    double elapsed_secs = double(m_end - m_begin);
    std::cout << "Time elapsed: " << (double)(elapsed_secs / CLOCKS_PER_SEC) << " seconds"<< std::endl;
}

void Info::setMethod(const std::string &method)
{
    m_method = method;
}

std::vector<long> & Info::getLeft()
{
    return m_left;
}

std::vector<long> & Info::getRight()
{
    return m_right;
}

void Info::pushLeft(const long & value)
{
    m_left.push_back(value);
}

void Info::pushRight(const long & value)
{
    m_right.push_back(value);
}
