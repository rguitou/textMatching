#ifndef INFO_H
#define INFO_H

#include <vector>
#include <string>
#include <iostream>
#include <ctime>

class Info // will be the return type of all algorithms to have more complete informations about similar texts
{
public:
    Info();
    void display();
    void setMethod(const std::string & method);
    std::vector<long>& getLeft();
    std::vector<long>& getRight();
    void pushLeft(const long & value);
    void pushRight(const long & value);

private:
    std::vector<long> m_left; // a list which contains all pair of similar text's IDs
    std::vector<long> m_right;
    std::string m_method; // name of the method which proved similarity
    std::clock_t m_begin;
    std::clock_t m_end;
};

#endif // INFO_H
