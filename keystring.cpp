#include "keystring.h"

KeyString::KeyString()
{
    m_biggestID = 0;
    m_base.reserve(1000);
    m_knownWords.reserve(10000);
    m_keyTabIDs.reserve(10000);
    m_tabKeyIDsBorders.reserve(2000);
    mailInit();// fill the m_base array
    wordsInit("output.csv", m_knownWords); // fill the m_knownWords array with already known words
    IDGeneration(); // fill the m_knownWords array
    IDTab(); // make a sequence of word's IDs with each text of m_base

}

KeyString::~KeyString()
{
}

// this function take a csv file to fill an array with its contents
void KeyString::wordsInit(const std::string & name, std::vector<std::string> & base)
{
    std::ifstream file;
    file.open(name);

    if(file.fail())
        exit(1);

    while(!file.eof()) // for every line of the file
    {
        std::string line;
        std::string text;
        long id = 0;
        std::getline(file, line); // stocking the current line in the variable "line"
        char *copy = new char[line.length() + 1]; // cast the variable "line" to a char array
        strcpy(copy, line.c_str());
        char *token = strtok(copy, ";"); // tokenizing the line
        while (token != NULL) {
            if(atol(token) != 0)
                id = atol(token); // if the tokenized part is a number, we consider that it is the id of the other string on the line
            else
            {
                text = token;
                base.push_back(text);
            }// filling the array
            token = strtok(NULL, ";");
        }
        if(name == "output.csv" && id > m_biggestID) // if we discover an id number is greater than the biggest ID
            m_biggestID = id; // we consider it as the biggest one
        delete [] copy;
        delete [] token;// deleting pointers
    }
    file.close();
}

// this function take the emails.csv file to fill an array with its contents
void KeyString::mailInit()
{
    std::ifstream file;
    file.open("emails.csv");

    if(file.fail())
        exit(1);

    std::string made = "";
    std::string line;

    int j = 0;
    long id = 0;
    while(!file.eof() && id < 1000) // for every line contained in the file
    {
        std::getline(file, line);
        for(unsigned int i = 0;i < line.length(); i++)
        {
            if(made == "or::_M_realloc_insert")
                made = "";
            if(line[i] == '"') // a text field is between two double-quotes
            {
                if(line[i] != ',' && made != "")// two text fields are separated by a comma
                {
                    if(j % 3 == 1) // insert one text field on two because of the filename field
                    {
                        ///////////////////////
                        m_base.push_back(made);
                        made = "";
                        id++;
                        ///////////////////////
                        //insertion
                    }
                    j++;
                }
            }
            else if((line[i] >= 65 && line[i] <= 123 && line[i] != 95) || line[i] == ' ')
                made += toupper(line[i]); // just letters and spaces are inserted
        }
    }
    file.close();
}

//this function is made to browse the "m_base" array to discover new words to add to the output
void KeyString::IDGeneration()
{
    char sep = ' ';
    char sep2 = '\0';
    std::string made; // a place to temporarily stock words
    for(long i = 0; i < m_base.size(); i++) // browsing texts
    {
        std::string text = m_base[i]; // stocking the current text
        std::vector<std::string> one; // a place to stock all words of the current text
        for(unsigned long i = 0; i <= text.length(); i++) // browsing all characters of the text
        {
            if(text[i] == sep || text[i] == sep2) // if the current character is a space
            {
                one.push_back(made);
                made = "";
            }
            else
            {
                made += text[i]; // we add each character to the current word
            }
        }
        for(int j = 0; j < one.size(); j++) // browsing each word of our array
        {
            bool found = false;
            long k = 0;
            while(k < m_knownWords.size() && !found) // while the "known words" array isn't browsed or the id of the current word find
            {
                if (m_knownWords[k] == one[j])
                    found = true; // finding the id of the current word
                k++;
            }
            if(!found) // if the current word is unknown
            {
                m_knownWords.push_back(one[j]); // we add it in the array
                m_biggestID++;
                writeToOutput(one[j]); // we write it on output.csv for the next time
            }
        }
    }
}


//this function write each unknown word on the output file
void KeyString::writeToOutput(const std::string & word)
{
    std::ofstream file;
    file.open("output.csv", std::fstream::app); // opening output.csv
    if(file.fail())
        exit(1);


    if (word[0] != '\0')//if the word isn't a null character
        file << std::to_string(m_biggestID) << ";" << word << "\n"; //we write it with its new id in the file
    file.close();
}


//this function generates a id number array from a text in the resource file
void KeyString::IDTab()
{
    std::ifstream file;
    file.open("textIDs.csv");// opening textIDs.csv

    if(file.fail())
        exit(1);


    while(!file.eof()) // for every line of the file
    {
        std::string line;
        std::getline(file, line); // stocking the current line in the variable "line"
        char *copy = new char[line.length() + 1]; // cast the variable "line" to a char array
        strcpy(copy, line.c_str());
        char *token = strtok(copy, ";"); // tokenizing the line
        while (token != NULL)
        {
            if(charContains(token, ',') && token[0] != '\0')
                parsing(token); // if the tokenized part is a number( and not a sequence of numbers), we consider that it is the id of the other string on the line
            token = strtok(NULL, ";");
        }
        delete [] copy;
        delete [] token;// deleting pointers
    }
    if( m_base.size() > m_tabKeyIDsBorders.size() / 2) // if there are more texts rows than ID rows
    {
        long maxBase = m_base.size();
        long maxKeyTabIDs = m_tabKeyIDsBorders.size() / 2 ;
        for(maxKeyTabIDs = maxKeyTabIDs + 1; maxKeyTabIDs < maxBase; maxKeyTabIDs++)
            generateTextID(maxKeyTabIDs); //we generate the missing ones
    }

    file.close();
}

void KeyString::generateTextID(const long & stringId)
{

    std::vector<std::string> cutted; //every word of the text
    std::vector<long> idTab; // id number array
    std::string sentence = m_base[stringId]; // the converted text

    //////////////////////////////////////////////////
    char sep = ' ';
    std::string made = "";
    for(unsigned long i = 0; i <= sentence.length(); i++)
    {
        if((sentence[i] == sep || sentence[i] == '\0') && made != "") // each word is a block in our set
        {
            cutted.push_back(made);
            made = ""; // initializing a new word
        }
        else if(sentence[i] != sep && sentence[i] != '\0')// we don't add null characters
        {
            made += sentence[i]; // we add each character to the current word
        }
    }

    long i;
    m_tabKeyIDsBorders.size() == 0  ? i = 0 : i = m_tabKeyIDsBorders.back();
    m_tabKeyIDsBorders.push_back(i);
    for(int j = 0; j < cutted.size(); j++) //for each word
    {
         //inserting the lower bound of the sequence
        long k = 0;
        bool found  = false;
        while (k < m_knownWords.size() && !found) // we try to find its corresponding ID
        {
            if (m_knownWords[k] == cutted[j]) // if we find it
            {
               idTab.push_back(k);// we push our word's ID in our tab
               found = true;
               i++;
            }
            k++;
        }
    }
    m_tabKeyIDsBorders.push_back(i);  // inserting the upper bound of the sequence
    /////////////////////////////////////////////
    // similar code in the IDGeneration function
    writeToTextIDFile(idTab, stringId);// writing our sequence on textIDs.csv
}

void KeyString::writeToTextIDFile(std::vector<long> idTab, const long & currentID)
{
    std::ofstream file;
    file.open("textIDs.csv", std::fstream::app); // opening textIDs.csv
    if(file.fail())
        exit(1);


    file << std::to_string(currentID) << ";"; //we write it with its new id in the file
    for(long i = 0; i < idTab.size(); i++)
        file << std::to_string(idTab[i]) << "," ;
    file << "\n"; //we write it with its new id in the file
    file.close();
}

std::string KeyString::findById(const long &stringID)
{
    //find a string by its corresponding ID in output.csv
    return m_knownWords[stringID];
}

std::vector<long> KeyString::getKeyTabIDs()
{
    return m_keyTabIDs;
}

std::vector<long> KeyString::getTabKeyIDsBorders()
{
    return m_tabKeyIDsBorders;
}

//this function was made to parse a sentence to put it in m_keyTabIDs
void KeyString::parsing(const std::string &parsed)
{
    long j;
    m_tabKeyIDsBorders.size() == 0  ? j = 0 : j = m_tabKeyIDsBorders.back();
    m_tabKeyIDsBorders.push_back(j);
    // parsed is our sequence of number used in the function IDTab (in a string)
    std::string made = "";
    for(unsigned long i = 0; i < parsed.length(); ++i) // for each character in parsed
    {
        if(parsed[i] != ',')
            made += parsed[i]; // if the current character isn't a comma we consider it as a number
        else
        {
            j++;
            long value = atol(made.c_str());
            m_keyTabIDs.push_back(value); // inserting the long integer which we created
            made = "";// initializing a new number
        }
    }
    m_tabKeyIDsBorders.push_back(j);
}

// this function check if a precise character is present in a text
bool KeyString::charContains(char *&container, const char &contained)
{
    long size = strlen(container);
    for(int i = 0; i < size; i++)
    {
        if(container[i] == contained)
            return true;
    }
    return false;
}
