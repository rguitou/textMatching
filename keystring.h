#ifndef KEYSTRING_H
#define KEYSTRING_H

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <stdio.h>
#include <string.h>
#include <array>

//#include "sorter.h"

class KeyString
{
public:
    KeyString();
    ~KeyString();

    void wordsInit(const std::string & name, std::vector<std::string> & base);
    void mailInit();
    void IDGeneration();
    void writeToOutput(const std::string & word);
    void generateTextID(const long & stringId);
    void IDTab();
    void parsing(const std::string & parsed);
    void writeToTextIDFile(std::vector<long> idTab, const long & currentID);

    bool charContains(char*  & container ,const char & contained);
    std::string findById(const long & stringID);
    std::vector<long> getKeyTabIDs();
    std::vector<long> getTabKeyIDsBorders();

private:
    std::vector<std::string> m_base; // a tab where figure the untreated texts and their IDs
    std::vector<std::string> m_knownWords; // a tab where figure every known words and their IDs
    std::vector<long> m_keyTabIDs; // an array where figure the treated texts where every word is replaced by his "m_knownWords ID"
    std::vector<long> m_tabKeyIDsBorders;
    int m_biggestID;
};

#endif // KEYSTRING_H
