#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <ctime>
#include <omp.h>

#include "keystring.h"
#include "info.h"
#include "sorter.h"


using namespace std;


int comparation(vector<long> & one, vector<long> & two, int sizeToExaminate)
{
    int counter = 0;
    long j = 0;
    for(long i = 0; i < sizeToExaminate; i++)
    {
        while (j < sizeToExaminate)
        {
            if(one[i] == two[j])
                counter++;
            j++;
        }
        j = 0;
    }
    cout << "Similarity: " << counter << endl;
    return counter;
}

vector<long> getSubArrayOf(std::vector<long> & completeArray, long beg, long end)
{
    vector<long> subArray;
    for(long i = beg; i < end; i++)
    {
        subArray.push_back(completeArray[i]);
    }
    return subArray;
}


Info naive(KeyString & k, const float & threshold)
{
    Info in;
#pragma omp parallel
    {
        long lowerBoundOne, upperBoundOne, lowerBoundTwo, upperBoundTwo;
        int cpt = 0;// useful for jaccard calculation (r ∩ s)
        int min = 0;
        vector<long> studiedSet = k.getKeyTabIDs();// m_keyTabIDs
        vector<long> borders = k.getTabKeyIDsBorders();
        vector<long> subArrayOne, subArrayTwo;

#pragma omp for schedule(static) ordered
        for(unsigned long i = 0; i < borders.size(); i++)
        {
            lowerBoundOne = borders[i];
            upperBoundOne = borders[i + 1];

            subArrayOne = getSubArrayOf(studiedSet, lowerBoundOne, upperBoundOne);

            for(unsigned long j = i + 2; j < borders.size(); j++) // we compare each texts to each other
            {
                lowerBoundTwo = borders[j];
                upperBoundTwo = borders[j + 1];
                (upperBoundOne - lowerBoundOne) > (upperBoundTwo - lowerBoundTwo) ? min = (upperBoundTwo - lowerBoundTwo) : min = (upperBoundOne - lowerBoundOne);

                subArrayTwo = getSubArrayOf(studiedSet, lowerBoundTwo, upperBoundTwo);
                cpt = comparation(subArrayOne, subArrayTwo, min);
                int totalSize = (upperBoundOne - lowerBoundOne) + (upperBoundTwo - lowerBoundTwo);
                float similarityPercentage = (float)(cpt) / (float)(totalSize - cpt); // jaccard calculation (r ∩ s) / (r ∪ s)
#pragma omp critical
                if(similarityPercentage > threshold)
                {
                    in.pushLeft(i/2);
                    in.pushRight(j/2);
                }
                cpt = 0;
                j++;
                subArrayTwo.clear();
            }
            i++;
            subArrayOne.clear();
        }
    }
    in.setMethod("naive");
    in.display();
    return in;
}

Info prefixFiltering(KeyString & k, float threshold)
{
    Info in;
#pragma omp parallel default(none) shared(k, in, threshold)
    {
        vector<long> borders = k.getTabKeyIDsBorders();
        vector<long> studiedSet = k.getKeyTabIDs();
        long min, prefixNumber, lowerBoundOne, upperBoundOne, lowerBoundTwo, upperBoundTwo; //prefixNumber will contains how many strings of each texts will be examinated
        int cpt = 0;
        // m_keyTabIDs
        vector<long> subArrayOne, subArrayTwo;
        subArrayOne.reserve(3000);
        subArrayTwo.reserve(3000);
#pragma omp for schedule(static) ordered
        for(unsigned long i = 0; i < borders.size(); i++)
        {
            lowerBoundOne = borders[i];
            upperBoundOne = borders[i + 1];
            subArrayOne = getSubArrayOf(studiedSet, lowerBoundOne, upperBoundOne);
            for(unsigned long j = i + 2; j < borders.size(); j++)
            {
                lowerBoundTwo = borders[j];
                upperBoundTwo = borders[j + 1];
                subArrayTwo = getSubArrayOf(studiedSet, lowerBoundTwo, upperBoundTwo);
                (upperBoundOne - lowerBoundOne) > (upperBoundTwo - lowerBoundTwo) ? min = (upperBoundTwo - lowerBoundTwo) : min = (upperBoundOne - lowerBoundOne); // we keep the minimum size in memory
                min > prefixNumber ? prefixNumber = long(((1.0 - threshold)*min) + 1) : prefixNumber = min; // documentation's formula
                if(subArrayOne.size() != 0 && subArrayTwo.size() != 0)
                    cpt = comparation(subArrayOne, subArrayTwo, prefixNumber);

                float similarityPercentage = (float)(cpt) / (float)(prefixNumber - cpt); // jaccard calculation (r ∩ s) / (r ∪ s)
                #pragma omp critical
                if(similarityPercentage > threshold)
                {
                    in.pushLeft(i/2);
                    in.pushRight(j/2);
                }
                j++;
                subArrayTwo.clear();
            }
            i++;
            subArrayOne.clear();
        }
    }
    in.setMethod("prefix filtering");
    in.display();
    return in;
}

Info lengthFiltering(KeyString & ke, const int & maxCharLength, const float & threshold)
{
    Info in;
#pragma omp parallel
    {

        long groupOne[maxCharLength], groupTwo[maxCharLength]; // in these vector arrays, indexes corresponds to word length
        //if we encounter a word with 5 letters we will increments groupOne[5] for example
        std::vector<long> studiedSet = ke.getKeyTabIDs();
        std::vector<long> borders = ke.getTabKeyIDsBorders();
        unsigned long lowerBoundGeneral, upperBoundGeneral, lowerBoundOne, upperBoundOne, lowerBoundTwo, upperBoundTwo;// if two examinated strings have a too different length we can prune them
        int cpt = 0;
        vector<long> subArrayOne, subArrayTwo;
#pragma omp for
        for(unsigned long i = 0; i < borders.size(); i++)
        {
            lowerBoundOne = borders[i];
            upperBoundOne = borders[i + 1];
            subArrayOne = getSubArrayOf(studiedSet, lowerBoundOne, upperBoundOne);

            int subArrayOneSize = borders[i + 1] - borders[i];
            lowerBoundGeneral = (long)((float)subArrayOneSize * threshold);
            upperBoundGeneral = (long)((float)subArrayOneSize / threshold);//documentation's formulas
            for(unsigned long j = i + 2; j < borders.size(); j++)
            {
                lowerBoundTwo = borders[j];
                upperBoundTwo = borders[j + 1];

                subArrayTwo = getSubArrayOf(studiedSet, lowerBoundTwo, upperBoundTwo);

                int subArrayTwoSize = borders[j + 1] - borders[j];
                if(subArrayTwoSize >= lowerBoundGeneral && subArrayTwoSize <= upperBoundGeneral) //if j string's length is between the bounds
                {

                    int currentWordI = ke.findById(studiedSet[i]).length();
                    int currentWordJ = ke.findById(studiedSet[j]).length();
                    if(currentWordI < maxCharLength)
                        groupOne[currentWordI]++;
                    if(currentWordJ < maxCharLength)
                        groupTwo[currentWordJ]++;
                    for(long l = 0; l < maxCharLength; l++)
                    {
                        if(groupOne[l] == groupTwo[l])// if they contains the same number of words of the same length
                            cpt++;
                    }
                }
                long size = subArrayOneSize + subArrayTwoSize;
                float similarityPercentage = (float)(cpt) / (float)(size - cpt);
                #pragma omp critical
                if(similarityPercentage >= threshold)
                {
                    in.pushLeft(i/2);
                    in.pushRight(j/2);
                }
                cpt = 0;
                for(int k = 0; k < maxCharLength; k++)
                    groupOne[k] = 0;
                j++;
                subArrayTwo.clear();
            }
            for(int l = 0; l < maxCharLength; l++)
                groupTwo[l] = 0;
            i++;
            subArrayOne.clear();
        }
    }
    in.setMethod("length filtering");
    in.display();
    return in;
}

Info lengthPrefixFiltering(KeyString & ke, const float & threshold)
{
    Info in;
#pragma omp parallel default(none) shared(ke, in, threshold)
    {
        vector<long> borders = ke.getTabKeyIDsBorders();
        vector<long> studiedSet = ke.getKeyTabIDs();
        long min, prefixNumber, lowerBoundOne, upperBoundOne, lowerBoundTwo, upperBoundTwo; //prefixNumber will contains how many strings of each texts will be examinated
        int cpt = 0;
        // m_keyTabIDs
        vector<long> subArrayOne, subArrayTwo;
        subArrayOne.reserve(3000);
        subArrayTwo.reserve(3000);
#pragma omp for schedule(static) ordered
        for(unsigned long i = 0; i < borders.size(); i++)
        {
            lowerBoundOne = borders[i];
            upperBoundOne = borders[i + 1];
            subArrayOne = getSubArrayOf(studiedSet, lowerBoundOne, upperBoundOne);
            int subArrayOneSize = borders[i + 1] - borders[i];

            int lowerBoundGeneral = (long)((float)subArrayOneSize * threshold);
            int upperBoundGeneral = (long)((float)subArrayOneSize / threshold);

            for(unsigned long j = i + 2; j < borders.size(); j++)
            {
                lowerBoundTwo = borders[j];
                upperBoundTwo = borders[j + 1];
                subArrayTwo = getSubArrayOf(studiedSet, lowerBoundTwo, upperBoundTwo);
                int subArrayTwoSize = borders[j + 1] - borders[j];
                if(subArrayTwoSize >= lowerBoundGeneral && subArrayTwoSize <= upperBoundGeneral)
                {
                    (upperBoundOne - lowerBoundOne) > (upperBoundTwo - lowerBoundTwo) ? min = (upperBoundTwo - lowerBoundTwo) : min = (upperBoundOne - lowerBoundOne); // we keep the minimum size in memory
                    min > prefixNumber ? prefixNumber = long(((1.0 - threshold)*min) + 1) : prefixNumber = min; // documentation's formula
                    if(subArrayOne.size() != 0 && subArrayTwo.size() != 0)
                        cpt = comparation(subArrayOne, subArrayTwo, prefixNumber);

                    float similarityPercentage = (float)(cpt) / (float)(prefixNumber - cpt); // jaccard calculation (r ∩ s) / (r ∪ s)
                    #pragma omp critical
                    if(similarityPercentage > threshold)
                    {
                        in.pushLeft(i/2);
                        in.pushRight(j/2);
                    }
                }
                j++;
                subArrayTwo.clear();
            }
            i++;
            subArrayOne.clear();
        }
    }
    in.setMethod("length and prefix filtering");
    in.display();
    return in;
}

int main()
{
    omp_set_nested(1);
    KeyString k;
    //naive(k, 0.95);
    //prefixFiltering(k, 0.95);
    //lengthFiltering(k, 15, 0.95);
    lengthPrefixFiltering(k, 0.95);

    return 0;
}
