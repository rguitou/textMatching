#include "sorter.h"

Sorter::Sorter(std::vector<long> & sorted)
{
    sort(&sorted);
}

Sorter::Sorter()
{

}

void Sorter::exchange(std::vector<long> &  one, long p1, long p2)
{
    long tmp = one[p1];
    one[p1] = one[p2];
    one[p2] = tmp;
}

long Sorter::partition(std::vector<long> & one, long beg, long end)
{
    long counter = beg;
    long pivot = one[beg];

    for(long i = beg+1; i<=end; i++)
    {
        if (one[i] < pivot)
        {
            counter++;
            exchange(one,counter,i);
        }
    }
    exchange(one,beg,counter);
    return counter;
}

void Sorter::sort(std::vector<long> & one, long beg, long end)
{
    if(beg < end)
    {
        long positionPivot = partition(one, beg, end);
        sort(one, beg, positionPivot - 1);
        sort(one, positionPivot+1, end);
    }
}

void Sorter::sort(std::vector<long>* one) // rapid sort (hoare)
{
    long length = one->size();// get size
    sort(*one,0,length-1);
}
