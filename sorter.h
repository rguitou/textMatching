#ifndef SORTER_H
#define SORTER_H

#include <iostream>
#include <string>
#include <vector>

class Sorter
{
public:
    Sorter(std::vector<long> & sorted);
    Sorter();
    void exchange(std::vector<long> & one, long p1, long p2);
    long partition(std::vector<long>&  one, long beg, long end);
    void sort(std::vector<long>&  one, long beg, long end);
    void sort(std::vector<long>* one);
};

#endif // SORTER_H
